import Component from "./Components/Component.js";
import Img from "./Components/Img.js";

const title2 = new Component("h1", null, "lol");
document.querySelector(".pageTitle").innerHTML = title2.render();

const title = new Component("h1", null, ["La ", "gucci", "carte"]);
document.querySelector(".pageTitle").innerHTML = title.render();

const img = new Component("img", {
  name: "src",
  value:
    "https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300",
});
document.querySelector(".pageContent").innerHTML = img.render();
// const c = new Component("article", { name: "class", value: "pizzaThumbnail" }, [
//   new Img(
//     "https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300"
//   ),
//   "Regina",
// ]);
//document.querySelector(".pageContent").innerHTML = c.render();
