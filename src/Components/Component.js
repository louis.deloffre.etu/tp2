export default class Component {
  tagName;
  children;
  attribute;
  constructor(tagName, attribute, children) {
    this.tagName = tagName;
    this.children = children;
    this.attribute = attribute;
  }

  render() {
    let path = `<${this.tagName}`;
    if (this.attribute == null && this.children == null) {
      path = `><${this.tagName}/>`;
    }
    if (this.attribute != null && this.children == null) {
      path += ` ${this.renderAttribute()} />`;
    }
    if (this.attribute != null && this.children != null) {
      path += `${this.renderAttribute()} />`;
    }
    if (this.children != null || this.children != undefined) {
      path += `${this.renderChildren()}`;
    }
    return path;
  }
  renderChildren() {
    let path = `<${this.tagName}>`;
    if (this.children instanceof Array) {
      for (let idx = 0; idx < this.children.length; idx++) {
        if (this.children[idx] instanceof Component) {
          path += this.children.render();
        } else {
          path += this.children[idx];
        }
      }
    }
    return path;
  }
  renderAttribute() {
    return `${this.attribute.name}="${this.attribute.value}"`;
  }
}
